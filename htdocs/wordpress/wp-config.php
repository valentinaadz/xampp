<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`M{P#&Q|9|cNSD;AErW+Bk`N^wvEANO G;15t3*biwkm|[c8Kf&BZj:{Ho%xxbN[');
define('SECURE_AUTH_KEY',  '{;!}h%X+r)y`GKWf`wj G.k`Y`LsBvCAuN,VrFE?O J/=5w%JO~bCr:c*4nNz?ve');
define('LOGGED_IN_KEY',    '{Jj-Qru?kw&-EX[R,R FSE9bpnHR0v8o}a3#{z@`B4eMU|LKw|-+8Opu~rW7FU>f');
define('NONCE_KEY',        'BVX~~g&}<~8V6L?kl|[7lVWS65weTJ2wL.2 a5#wTK7Y!)]B!.><3#LYRyCJ+wcN');
define('AUTH_SALT',        '} xi};Xk|l|c%FdJA$4cg+Pp{rF+ S3 LG>3S~-H+Z+^a{:|XyH0M-C}}zp<r[AB');
define('SECURE_AUTH_SALT', 'PAmBTTYdk8ANj .3D2q#-i+y6mS+cz)+%adI).@(w+Zgei)9E=z%F9N3tQ|J=u&z');
define('LOGGED_IN_SALT',   'sHVEQiTjQd+1`S4pX&-Lb0,K;bYcz~>w0YpNg!?JR`k#<!Jt~=2IOf~3{D.=c[I|');
define('NONCE_SALT',       'Y|j|YH8@KA^{i5S.nA+JI=PFg)09]%H9+B0Ydk0bmU5S(f;2W&k*a#KayLPT3]:C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
